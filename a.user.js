// ==UserScript==
// @name Userscript Test
// @namespace http://example.com/
// @version 1.0.0
// @description Test
// @author
// @grant none
// @downloadURL https://seangenabe.gitlab.io/userscript-test/a.user.js
// @updateURL https://seangenabe.gitlab.io/userscript-test/a.user.js
// @run-at document-start
// ==/UserScript==

import "https://seangenabe.gitlab.io/userscript-test/b.js"
